*******************************************************
    README.txt for solr_google_like.module for Drupal
*******************************************************
Make your Drupal Search' look and feel more Google-ish

The SOLR Google Like module gives users a a closer look and feel 
with Google-style search results.

The module depends on Apache SOLR module.

To install the SOLR Google Like module:

- Put the solr_google_like directory in your modules directory.
- Navigate to administer --> modules, and enable the SOLR Google Like module.
- If the Apache Solr module is not installed it will be. You need to have 
a functionnal Apache Solr Search to get some benefit of the SOLR Google 
Like module. Refer to the Apache Solr module to deal with it : 
https://drupal.org/project/apachesolr

TECHNICAL NOTES:
The SOLR Google Like override some Apache Solr module configuration variables,
calculating the number of results and the age of the content found.
The module provides a css and overrides both search templates
search-result.tpl.php and search-results.tpl.php.

Feel free to override css and templates in your theme.

The SOLR Google Like module was developed by PROBESYS.
